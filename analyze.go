package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

const (
	goModFilename    = "go.mod"
	defaultGoPkgPath = "/go/src/app"
	envGoPkgPath     = "GOSEC_GO_PKG_PATH"
	flagCompile      = "compile"
	flagGoPkgPath    = "go-pkg-path"
	pathOutput       = "/tmp/gosec.json"
	pathGosec        = "/bin/gosec"
)

var pathGoPkg = defaultGoPkgPath

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagCompile,
			Usage:   "Compile source code. It's not needed if the code is already compiled.",
			EnvVars: []string{"COMPILE"},
			Value:   true,
		},
		&cli.StringFlag{
			Name:    flagGoPkgPath,
			Usage:   "(Experimental) Define path where go project is copied to and compiled.",
			Value:   defaultGoPkgPath,
			EnvVars: []string{envGoPkgPath},
		},
	}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	var cmd *exec.Cmd
	var err error
	var output = []byte{}

	var goModPath = filepath.Join(projectPath, goModFilename)

	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		return cmd
	}

	pathGoPkg = c.String(flagGoPkgPath)
	log.Infof("using %s for the go pkg path", pathGoPkg)

	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	rulesetConfig, err := ruleset.Load(rulesetPath, "gosec")
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return nil, err
		}
	}

	if c.Bool(flagCompile) {
		// if go modules are not enabled, fall back to copying modules to GOPATH
		if fileExistsAtPath(goModPath) == nil {
			log.Info("go modules detected")
			pathGoPkg = projectPath
		} else {
			// We don't control the directory where the source code is mounted
			// but Go requires the code to be within $GOPATH.
			// We could create a symlink but that wouldn't work with Gosec,
			// so we have to copy all the project source code
			// to some directory under $GOPATH/src.
			log.Info("Copying modules into path...")
			cmd = setupCmd(exec.Command("cp", "-r", projectPath, pathGoPkg))
			output, err := cmd.CombinedOutput()
			log.Debugf("%s\n%s", cmd.String(), output)
			if err != nil {
				return nil, err
			}
		}

		// Gosec needs the dependency to be fetched.
		log.Info("Fetching dependencies...")
		cmd = setupCmd(exec.Command("go", "get", "-d", "./..."))
		cmd.Dir = pathGoPkg
		output, err = cmd.CombinedOutput()
		if err != nil {
			log.Errorf("%s\n%s", cmd.String(), output)
			return nil, err
		}
		log.Debugf("%s\n%s", cmd.String(), output)
	}

	// Set up basic gosec arguments
	gosecArgs := []string{"-fmt=json", "-out=" + pathOutput, "./..."}

	if rulesetConfig != nil && len(rulesetConfig.Passthrough) > 0 {
		configPath, err := ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
		if err != nil {
			return nil, err
		}

		abs := configPath
		if !filepath.IsAbs(configPath) {
			abs = filepath.Join(projectPath, configPath)
		}

		info, err := os.Stat(abs)
		if err != nil {
			return nil, err
		}
		// check if a configuration file has been generated
		if !info.IsDir() {
			gosecArgs = append([]string{"-conf", abs}, gosecArgs...)
		} else {
			log.Errorf("gosec only supports configuration files ... skipping '%s'", abs)
		}
	}

	log.Info("Running gosec...")
	cmd = setupCmd(exec.Command(pathGosec, gosecArgs...))
	cmd.Dir = pathGoPkg
	output, err = cmd.CombinedOutput()
	// NOTE: Gosec exit with status 1 if some vulnerabilities have been found.
	if err != nil && cmd.ProcessState.ExitCode() > 1 {
		log.Errorf("%s\n%s", cmd.String(), output)
		return nil, err
	}
	log.Debugf("%s\n%s", cmd.String(), output)
	return os.Open(pathOutput)
}

func fileExistsAtPath(configPath string) error {
	st, err := os.Stat(configPath)
	if err != nil {
		return err
	} else if st.IsDir() {
		return fmt.Errorf("%q is a directory", configPath)
	}
	return nil
}
